﻿// See https://aka.ms/new-console-template for more information
using System;
using Microsoft.AspNetCore.SignalR.Client;
namespace SignalRClient;

class Program
{
    static async Task Main(string[] args)
    {
        var handler = new HttpClientHandler();
        handler.ServerCertificateCustomValidationCallback =
            HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
        var connection = new HubConnectionBuilder()
            .WithUrl("https://localhost:7128/chatHub", options =>
            {
                options.HttpMessageHandlerFactory = _ => handler;
            })
            .Build();

        connection.On<string, string>("ReceiveMessage", (user, message) =>
        {
            Console.WriteLine($"{user}: {message}");
        });
        
        var eventhubConnection = new HubConnectionBuilder()
            .WithUrl("https://localhost:5002/eventHub", options =>
            {
                options.HttpMessageHandlerFactory = _ => handler;
            })
            .Build();

        connection.On<string, string>("ReceiveMessage", (user, message) =>
        {
            Console.WriteLine($"{user}: {message}");
        });
        var dataConnection = new HubConnectionBuilder()
            .WithUrl("https://localhost:5000/InfoHub", options =>
            {
                options.HttpMessageHandlerFactory = _ => handler;
            })
            .Build();

        dataConnection.On<string, string>("ReceiveData", (user, message) =>
        {
            Console.WriteLine($"InfoHub - {user}: {message}");
        });
        
        try
        {
            Console.WriteLine("Attempting connection to hub");
            await connection.StartAsync();
            Console.WriteLine("Connected to hub");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error connecting to hub: {ex.Message}");
        }
        
        while (true)
        {
            var message = Console.ReadLine();
            await connection.InvokeAsync("SendMessage", "Console Client", message);
        }
    }
}
